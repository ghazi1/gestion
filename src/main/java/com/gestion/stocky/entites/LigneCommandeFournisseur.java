package com.gestion.stocky.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="lignecommandefournisseur")
public class LigneCommandeFournisseur implements Serializable  {
	@Id
	@GeneratedValue
	private Long idLigneCommandeFournisseur;
	 @ManyToOne 
	 @JoinColumn(name="idArticle")
	private Article article;
	 @ManyToOne 
	 @JoinColumn(name="idcommandefournisseur")
	private CommandeFournisseur commandefournisseur;
	 //G&S
	public Long getIdLigneCommandeFournisseur() {
		return idLigneCommandeFournisseur;
	}
	public void setIdLigneCommandeFournisseur(Long idLigneCommandeFournisseur) {
		this.idLigneCommandeFournisseur = idLigneCommandeFournisseur;
	}
	public Article getArticle() {
		return article;
	}
	public void setArticle(Article article) {
		this.article = article;
	}
	public CommandeFournisseur getCommandefournisseur() {
		return commandefournisseur;
	}
	public void setCommandefournisseur(CommandeFournisseur commandefournisseur) {
		this.commandefournisseur = commandefournisseur;
	}
	 

}
