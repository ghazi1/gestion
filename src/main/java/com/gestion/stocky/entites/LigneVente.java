package com.gestion.stocky.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="lignevente")
public class LigneVente implements Serializable {
	@Id
	@GeneratedValue
	private Long idLignevente;
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	@ManyToOne
	@JoinColumn(name="vente")
	private Vente vente;
	public Long getIdLignevente() {
		return idLignevente;
	}
	public void setIdLignevente(Long idLignevente) {
		this.idLignevente = idLignevente;
	}
	public Article getArticle() {
		return article;
	}
	public void setArticle(Article article) {
		this.article = article;
	}
	
	
	
	
}
