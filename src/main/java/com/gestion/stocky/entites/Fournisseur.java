package com.gestion.stocky.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="fournisseur")
public class Fournisseur implements Serializable {
	@Id
	@GeneratedValue
	private Long idFournisseur;
	private String nom;
	private String prenom;
	private String adress;
	private String photo;
	private String mail;
	@OneToMany(mappedBy = "fournisseur")
	private List<CommandeFournisseur>commandefournisseurs;
	//const
	public Fournisseur() {
		super();
	}
	//getters & setters
	public Long getIdfournisseur() {
		return idFournisseur;
	}
	public void setIdfournisseur(Long idfournisseur) {
		this.idFournisseur = idfournisseur;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public Long getIdFournisseur() {
		return idFournisseur;
	}
	public void setIdFournisseur(Long idFournisseur) {
		this.idFournisseur = idFournisseur;
	}
	public List<CommandeFournisseur> getCommandefournisseurs() {
		return commandefournisseurs;
	}
	public void setCommandefournisseurs(List<CommandeFournisseur> commandefournisseurs) {
		this.commandefournisseurs = commandefournisseurs;
	}
	
}
