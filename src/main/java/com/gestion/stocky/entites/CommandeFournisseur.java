package com.gestion.stocky.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="commandefournisseur")
public class CommandeFournisseur implements Serializable {
	@Id
	@GeneratedValue
	private Long idcommandefournisseur;
	@Temporal(TemporalType.TIMESTAMP)//la date et l'heure
	private Date dateCommande;
	 @ManyToOne
	    @JoinColumn(name="idFournisseur")
	private Fournisseur fournisseur;
	 @OneToMany(mappedBy ="commandefournisseur" )
	private List<LigneCommandeFournisseur>lignecomandefournisseurs;
	 //G&S
	public Long getIdcommandefournisseur() {
		return idcommandefournisseur;
	}
	public void setIdcommandefournisseur(Long idcommandefournisseur) {
		this.idcommandefournisseur = idcommandefournisseur;
	}
	public Date getDateCommande() {
		return dateCommande;
	}
	public void setDateCommande(Date dateCommande) {
		this.dateCommande = dateCommande;
	}
	public Fournisseur getFournisseur() {
		return fournisseur;
	}
	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}
	public List<LigneCommandeFournisseur> getLignecomandefournisseurs() {
		return lignecomandefournisseurs;
	}
	public void setLignecomandefournisseurs(List<LigneCommandeFournisseur> lignecomandefournisseurs) {
		this.lignecomandefournisseurs = lignecomandefournisseurs;
	}
	 
}
