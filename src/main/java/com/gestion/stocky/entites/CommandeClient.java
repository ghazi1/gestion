package com.gestion.stocky.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="commandeclient")
public class CommandeClient implements Serializable {
	@Id
	@GeneratedValue
	private Long idCommandeclient;
	private String code;
	@Temporal(TemporalType.TIMESTAMP)//la date et l'heure
	private Date dateCommande;
	 @ManyToOne
	    @JoinColumn(name="idClient")
	private Client client;
	 @OneToMany(mappedBy ="commandeclient" )
	private List<LigneCommandeClient>lignecomandeclients;
	public Long getIdCommandeclient() {
		return idCommandeclient;
	}
	public void setIdCommandeclient(Long idCommandeclient) {
		this.idCommandeclient = idCommandeclient;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Date getDateCommande() {
		return dateCommande;
	}
	public void setDateCommande(Date dateCommande) {
		this.dateCommande = dateCommande;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public List<LigneCommandeClient> getLignecomandeclients() {
		return lignecomandeclients;
	}
	public void setLignecomandeclients(List<LigneCommandeClient> lignecomandeclients) {
		this.lignecomandeclients = lignecomandeclients;
	}
	
	
}
