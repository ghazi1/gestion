package com.gestion.stocky.entites;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="lignecommandeclient")
public class LigneCommandeClient implements Serializable {
	@Id
	@GeneratedValue
	private Long idLigneCommandeClinet;
	 @ManyToOne 
	 @JoinColumn(name="idArticle")
	private Article article;
	 @ManyToOne 
	 @JoinColumn(name="idCommandeclient")
	private CommandeClient commandeclient;
	public Long getIdLigneCommandeClinet() {
		return idLigneCommandeClinet;
	}
	public void setIdLigneCommandeClinet(Long idLigneCommandeClinet) {
		this.idLigneCommandeClinet = idLigneCommandeClinet;
	}
	public Article getArticle() {
		return article;
	}
	public void setArticle(Article article) {
		this.article = article;
	}
	public CommandeClient getCommandeclient() {
		return commandeclient;
	}
	public void setCommandeclient(CommandeClient commandeclient) {
		this.commandeclient = commandeclient;
	}
	
}
